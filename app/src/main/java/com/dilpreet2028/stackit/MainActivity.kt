package com.dilpreet2028.stackit

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.dilpreet2028.stackit.demo.FirstStackView
import com.dilpreet2028.stackit.demo.FourStackView
import com.dilpreet2028.stackit.demo.SecondStackView
import com.dilpreet2028.stackit.demo.ThirdStackView
import com.dilpreet2028.stackit.sdk.StackLayout

class MainActivity : AppCompatActivity() {

    private lateinit var stackLayout: StackLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        stackLayout = findViewById(R.id.stack_layout)
        initStacks()
    }

    private fun initStacks() {
        val firstStackView = FirstStackView()
        val secondStackView = SecondStackView()
        val thirdStackView = ThirdStackView()
        val fourthStackView = FourStackView()
        stackLayout.init(arrayOf(firstStackView, secondStackView, thirdStackView, fourthStackView),
            object : StackLayout.Callback {
                override fun onFlowEnded() {
                    // we have completed the whole flow can add navigation flow here
                }
            })
    }
}