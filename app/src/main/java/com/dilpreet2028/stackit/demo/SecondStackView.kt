package com.dilpreet2028.stackit.demo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dilpreet2028.stackit.R
import com.dilpreet2028.stackit.sdk.StackView

class SecondStackView : StackView() {

    override fun createHeaderView(layoutInflater: LayoutInflater, parent: ViewGroup): View {
        return layoutInflater.inflate(R.layout.layout_second_header, parent, false)
    }

    override fun onHeaderViewCreated(view: View) {
    }

    override fun createView(layoutInflater: LayoutInflater, parent: ViewGroup): View {
        return layoutInflater.inflate(R.layout.layout_second_view, parent, false)
    }

    override fun onViewCreated(view: View) {
    }

    override fun cta(): String = "Go to the Third"

    override fun submitClicked() {
        // same here
    }

}