package com.dilpreet2028.stackit.sdk

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


enum class StackState {
    EXPANDED,
    COLLAPSED
}


abstract class StackView {

    var state = StackState.COLLAPSED
    abstract fun createHeaderView(layoutInflater: LayoutInflater, parent: ViewGroup): View
    abstract fun onHeaderViewCreated(view: View)
    abstract fun createView(layoutInflater: LayoutInflater, parent: ViewGroup): View
    abstract fun onViewCreated(view: View)
    abstract fun cta(): String
    abstract fun submitClicked()
}