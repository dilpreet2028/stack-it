package com.dilpreet2028.stackit.sdk

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.Button
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import com.dilpreet2028.stackit.R
import kotlin.math.roundToInt


class StackLayout : FrameLayout {

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    companion object {
        const val COLLAPSED_HEIGHT = 96
    }

    interface Callback {
        fun onFlowEnded()
    }

    private var expandedPosition: Int = 0
    private var curPosition: Int = 0
    private var views = arrayOf<StackView>()
    private var callback: Callback? = null
    private lateinit var stackGroup: FrameLayout
    private lateinit var confirmButton: Button

    fun init(views: Array<StackView>, callback: Callback?) {
        if (views.size < 2 || views.size > 4) {
            throw IllegalArgumentException("Stacks views should be at-least 2 and at-most 4")
        }
        this.views = views
        this.callback = callback
        setupLayout()
        addExpandedView(0)
    }

    private fun setupLayout() {
        stackGroup = FrameLayout(context)
        confirmButton = Button(context)
        val param = LayoutParams(LayoutParams.MATCH_PARENT, dpToPx(88))
        param.gravity = Gravity.BOTTOM
        confirmButton.layoutParams = param
        confirmButton.setBackgroundResource(R.drawable.ic_top_round_corners)
        confirmButton.backgroundTintList =
            ContextCompat.getColorStateList(context, R.color.purple_700)
        stackGroup.layoutParams =
            LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)

        addView(stackGroup)
        addView(confirmButton)

        confirmButton.setOnClickListener {
            if (curPosition + 1 == views.size) {
                // end it here, we were on the last page
                callback?.onFlowEnded()
                return@setOnClickListener
            }
            if (curPosition == expandedPosition) {
                addExpandedView(curPosition + 1)
                collapseView(curPosition)
                expandedPosition++
                curPosition++
            } else {
                collapseView(curPosition)
                expandCurrentAndRestDown(++curPosition)
            }

        }
    }

    private fun collapseView(index: Int) {
        stackGroup.removeViewAt(index)
        addCollapsedView(index)
    }

    private fun addCollapsedView(index: Int) {
        val layoutInflater = LayoutInflater.from(context)
        val view = views[index].createHeaderView(layoutInflater, this)
        view.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
        view.backgroundTintList = ColorStateList.valueOf(getColor(0.88f + (0.04f * index)))
        view.setBackgroundResource(R.drawable.ic_top_round_corners)
        view.y = (dpToPx(COLLAPSED_HEIGHT) * index).toFloat()
        view.tag =
            index // can be used to get the index when adding support for clicking on the collapsed view
        stackGroup.addView(view, index)
        views[index].onHeaderViewCreated(view)
        view.setOnClickListener {
            val newPos = it.tag as Int
            collapseView(curPosition)
            collapseAndMoveUp(curPosition + 1, newPos)
            expandCurrentAndRestDown(newPos)
        }
    }

    private fun addExpandedView(index: Int) {
        val layoutInflater = LayoutInflater.from(context)
        val view = views[index].createView(layoutInflater, this)
        view.setBackgroundResource(R.drawable.ic_top_round_corners)
        view.y = height.toFloat()
        stackGroup.addView(view, index)
        views[index].onViewCreated(view)
        view.animate()
            .y((dpToPx(COLLAPSED_HEIGHT) * index).toFloat())
            .setDuration(150L)
            .withEndAction {
                view.layoutParams =
                    LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
            }
            .start()

        confirmButton.text = views[index].cta()
    }

    private fun collapseAndMoveUp(from: Int, to: Int) {
        for (i in from until to) {
            collapseView(i)
            stackGroup.getChildAt(i)
                .animate()
                .y((dpToPx(COLLAPSED_HEIGHT) * i).toFloat())
                .setDuration(130)
                .start()
        }
    }

    private fun expandCurrentAndRestDown(index: Int) {
        // expand current with expanded
        // iterate over index + 1 ... expandedPositions and update animate them to move down

        stackGroup.removeViewAt(index)
        addExpandedView(index)
        curPosition = index
        val heightOfFrame = stackGroup.height

        for (i in (index + 1)..expandedPosition) {
            stackGroup.getChildAt(i)
                .animate()
                .y((heightOfFrame - (dpToPx(COLLAPSED_HEIGHT) * (expandedPosition - i + 2))).toFloat())
                .setDuration(130)
                .start()
        }

    }

    private fun dpToPx(dp: Int): Int {
        val displayMetrics = context.resources.displayMetrics
        return (dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT)).roundToInt()
    }

    private fun getColor(intensity: Float): Int {
        val color = Color.parseColor("#14374f")
        val hsv = FloatArray(3)
        Color.colorToHSV(color, hsv)
        hsv[2] *= intensity
        return Color.HSVToColor(hsv)

    }
}